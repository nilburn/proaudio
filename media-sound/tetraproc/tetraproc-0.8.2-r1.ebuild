# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit eutils toolchain-funcs

RESTRICT="mirror"

DESCRIPTION="A-format to B-format signal converter for tetrahedral Ambisonic microphones"
HOMEPAGE="http://kokkinizita.linuxaudio.org/linuxaudio/"
SRC_URI="http://kokkinizita.linuxaudio.org/linuxaudio/downloads/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=">=media-libs/libclthreads-2.4.0
	>=media-libs/libclxclient-3.6.1
	media-libs/libpng:0
	media-libs/libsndfile
	sci-libs/fftw:3.0
	virtual/jack
	x11-libs/libX11
	x11-libs/libXft"
RDEPEND="${DEPEND}"

DOCS=( ../AUTHORS ../README ../blockdiagram.pdf )

PATCHES=( "${FILESDIR}/${P}-makefile.patch" )

S="${S}/source"

src_compile() {
	LDFLAGS="$(pkg-config --libs gthread-2.0) $LDFLAGS" \
	CXX="$(tc-getCXX)" emake PREFIX="/usr" LIBDIR="$(get_libdir)"
}
