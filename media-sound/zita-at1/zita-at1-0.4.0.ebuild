# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit toolchain-funcs

DESCRIPTION="An 'autotuner', normally used to correct the pitch of out of tune vocal tracks."
HOMEPAGE="http://kokkinizita.linuxaudio.org/linuxaudio/"
SRC_URI="http://kokkinizita.linuxaudio.org/linuxaudio/downloads/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=">=media-libs/libclthreads-2.4.0
	>=media-libs/libclxclient-3.6.1
	>=media-libs/zita-resampler-1.1.0
	virtual/jack
	media-libs/libpng
	sci-libs/fftw:3.0
	x11-libs/cairo
	x11-libs/libX11
	x11-libs/libXft"
RDEPEND="${RDEPEND}"

S="${WORKDIR}/${P}/source"
RESTRICT="mirror"

DOCS=(../AUTHORS ../README)
HTML_DOCS=(../doc/)

PATCHES=( "${FILESDIR}/${P}-Makefile.patch" )

src_compile() {
	default CXX="$(tc-getCXX)" PREFIX="${EPREFIX}/usr"
}
