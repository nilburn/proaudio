# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit qmake-utils

DESCRIPTION="JMess can save/load an XML file with all the current jack connections"
HOMEPAGE="https://ccrma.stanford.edu/groups/soundwire/software/jmess/"
SRC_URI="http://jmess-jack.googlecode.com/files/${P}.tar.gz"

RESTRICT="mirror"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="debug"

DEPEND="virtual/jack
	dev-qt/qtcore"
RDEPEND="${DEPEND}"

S="${WORKDIR}/${P}/src"

DOCS=("${WORKDIR}/${P}/INSTALL.txt" "${WORKDIR}/${P}/TODO.txt")

src_prepare() {
	default
	sed -e 's/toAscii/toLocal8Bit/g' -i JMess.cpp || die
	# sed -e "/^DESTDIR/s%= .*%= ${D}%" -i jmess.pro || die
	sed -e "/^target.path = /s%= .*%= ${D}/usr/bin%" -i jmess.pro || die
}

src_configure() {
	eqmake5
}

src_compile() {
	emake release
}

src_install() {
	emake release-install
	einstalldocs
}
