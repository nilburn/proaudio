# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

PYTHON_COMPAT=( python2_7 )

inherit python-r1

REQUIRED_USE="python? ( ${PYTHON_REQUIRED_USE} )"

RESTRICT="mirror"
DESCRIPTION="SoundFont editor library from the Swami project"
HOMEPAGE="http://swami.sourceforge.net/"
SRC_URI="mirror://sourceforge/swami/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="debug nls python"
PATCHES=( "${FILESDIR}/fix_gobject_linking.patch" )

RDEPEND="python? ( ${PYTHON_DEPS} )
	>=media-libs/libsndfile-1.0.0
	>=dev-libs/glib-2.0
	python? ( dev-python/pygtk[${PYTHON_USEDEP}] )"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

DOCS=( AUTHORS ChangeLog NEWS README )

src_configure() {
	python_setup

	econf --disable-rpath \
		--with-pic \
		$(use_enable debug) \
		$(use_enable nls) \
		$(use_enable python)
}
